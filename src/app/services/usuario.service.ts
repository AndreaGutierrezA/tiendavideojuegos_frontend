import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { IUsuario } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  endpoint = 'usuarios';
  url = 'http://localhost:3001/';

  constructor(
    private http: HttpClient,
  ) {
  }

  getAll(): Observable<IUsuario[] | null> {
    return this.http.get<IUsuario[] | null>(this.url + this.endpoint, { observe: 'response' }).pipe(
      map((response) => {
        return response?.body;
      })
    );
  }
  post(payload: object): Observable<IUsuario[] | null> {
    return this.http.post<IUsuario[] | null>(this.url + this.endpoint, payload, { observe: 'response' }).pipe(
      map((response) => {
        return response.body;
      })
    );
  }
}
