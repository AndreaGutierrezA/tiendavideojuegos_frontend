import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { ICarrito } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {
  endpoint = 'facturas';
  url = 'http://localhost:3001/';

  constructor(
    private http: HttpClient,
  ) {
  }
  getAll(): Observable<ICarrito[] | null> {
    return this.http.get<ICarrito[]| null>(this.url + this.endpoint, { observe: 'response' }).pipe(
      map((response) => {
        return response?.body;
      })
    );
  }
}
