import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { IVideoJuego } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class VideojuegosService {
  endpoint = 'videojuegos';
  url = 'http://localhost:3001/';

  constructor(
    private http: HttpClient,
  ) {
  }

  getAll(): Observable<IVideoJuego[] | null> {
    return this.http.get<IVideoJuego[] | null>(this.url + this.endpoint, { observe: 'response' }).pipe(
      map((response) => {
        return response?.body;
      })
    );
  }
  getOne(id: string): Observable<IVideoJuego| null> {
    return this.http.get<IVideoJuego | null>(this.url + this.endpoint + '/' + id, { observe: 'response' }).pipe(
      map((response) => {
        return response.body;
      })
    );
  }
  post(payload: object): Observable<IVideoJuego | null> {
    return this.http.post<IVideoJuego | null>(this.url + this.endpoint, payload, { observe: 'response' }).pipe(
      map((response) => {
        return response.body;
      })
    );
  }

  comprar(payload: {cantidad: string, videoJuegoId:string}): Observable<any> {
    return this.http.post<any>(this.url + this.endpoint + '/comprar', payload, { observe: 'response' }).pipe(
      map((response) => {
        return response;
      })
    );
  }
}
