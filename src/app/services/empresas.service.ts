import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { IEmpresaDesarrolladora } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {
  endpoint = 'empresas';
  url = 'http://localhost:3001/';

  constructor(
    private http: HttpClient,
  ) {
  }

  getAll(): Observable<IEmpresaDesarrolladora[] | null> {
    return this.http.get<IEmpresaDesarrolladora[] | null>(this.url + this.endpoint, { observe: 'response' }).pipe(
      map((response) => {
        return response?.body;
      })
    );
  }
}
