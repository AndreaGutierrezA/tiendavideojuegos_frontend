import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICarrito, IVideoJuego } from '../interfaces/interface';
import { Observable, map, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarritoCompraService {
  endpoint = 'carrito';
  url = 'http://localhost:3001/';

  // servicio reactivo
  private contador$: Subject<number> = new Subject();

  constructor(
    private http: HttpClient,
  ) {
  }

  get contador(): Observable<number> {
    return this.contador$.asObservable();
  }

  setContador(contador: number): void {
    this.contador$.next(contador);
  }

  get(id: string): Observable<ICarrito | null> {
    return this.http.get<ICarrito | null>(this.url + this.endpoint + '/byUser/' + id, { observe: 'response' }).pipe(
      map((response) => {
        return response?.body;
      })
    );
  }

  put(id: string | undefined): Observable<ICarrito | null> {
    return this.http.put<ICarrito | null>(this.url + this.endpoint + '/comprar/' + id, { observe: 'response' }).pipe(
      map((response) => {
        return response;
      })
    );
  }
  delete(id: string | undefined): Observable<ICarrito | null> {
    return this.http.delete<ICarrito | null>(this.url + this.endpoint + '/eliminar/' + id, { observe: 'response' }).pipe(
      map((response) => {
        return response.body;
      })
    );
  }
}
