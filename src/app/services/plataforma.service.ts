import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPlataforma } from '../interfaces/interface';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PlataformaService {

  endpoint = 'plataformas';
  url = 'http://localhost:3001/';

  constructor(
    private http: HttpClient,
  ) {
  }

  getAll(): Observable<IPlataforma[] | null> {
    return this.http.get<IPlataforma[] | null>(this.url + this.endpoint, { observe: 'response' }).pipe(
      map((response) => {
        return response?.body;
      })
    );
  }
}
