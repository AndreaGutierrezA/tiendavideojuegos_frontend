import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FrmVideoJuegoComponent } from './components/frm-video-juego/frm-video-juego.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { ListaEmpresasComponent } from './components/lista-empresas/lista-empresas.component';
import { ListaVideoJuegosComponent } from './components/lista-video-juegos/lista-video-juegos.component';
import { DetalleEmpresaComponent } from './components/detalle-empresa/detalle-empresa.component';
import { DetalleJuegoComponent } from './components/detalle-juego/detalle-juego.component';
import { AdminVideoJuegosComponent } from './components/admin-video-juegos/admin-video-juegos.component';
import { FormUsuarioComponent } from './components/form-usuario/form-usuario.component';
import { ListaCarritoComponent } from './components/lista-carrito/lista-carrito.component';

const routes: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'empresa/:idPlataforma', component: ListaEmpresasComponent },
  { path: 'detalleEmpresa/:idEmpresa', component: DetalleEmpresaComponent },
  { path: 'detalleJuego/:idJuego', component: DetalleJuegoComponent },
  { path: 'juego/:idEmpresa', component: ListaVideoJuegosComponent },
  { path: 'adminVideoJuegos', component: AdminVideoJuegosComponent },
  { path: 'formJuego', component: FrmVideoJuegoComponent },
  { path: 'formUsuario', component: FormUsuarioComponent },
  { path: 'listaCarrito', component: ListaCarritoComponent },
  { path: '**', redirectTo: '/inicio' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
