import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EncabezadoComponent } from './components/encabezado/encabezado.component';
import { FrmVideoJuegoComponent } from './components/frm-video-juego/frm-video-juego.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListaPlataformasComponent } from './components/lista-plataformas/lista-plataformas.component';
import { InicioComponent } from './paginas/inicio/inicio.component';
import { ListaEmpresasComponent } from './components/lista-empresas/lista-empresas.component';
import { ListaVideoJuegosComponent } from './components/lista-video-juegos/lista-video-juegos.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { DetalleJuegoComponent } from './components/detalle-juego/detalle-juego.component';
import { DetalleEmpresaComponent } from './components/detalle-empresa/detalle-empresa.component';
import { RatingModule } from 'ng-starrating';
import { HttpClientModule } from '@angular/common/http';
import { AdminVideoJuegosComponent } from './components/admin-video-juegos/admin-video-juegos.component';
import { FormUsuarioComponent } from './components/form-usuario/form-usuario.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CarritoCompraComponent } from './components/carrito-compra/carrito-compra.component';
import { ListaCarritoComponent } from './components/lista-carrito/lista-carrito.component';
@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    FrmVideoJuegoComponent,
    ListaPlataformasComponent,
    InicioComponent,
    ListaEmpresasComponent,
    ListaVideoJuegosComponent,
    CarouselComponent,
    DetalleJuegoComponent,
    DetalleEmpresaComponent,
    AdminVideoJuegosComponent,
    FormUsuarioComponent,
    CarritoCompraComponent,
    ListaCarritoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RatingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(
      {
        timeOut: 800,
        progressBar: true,
        progressAnimation: 'increasing'
      }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
