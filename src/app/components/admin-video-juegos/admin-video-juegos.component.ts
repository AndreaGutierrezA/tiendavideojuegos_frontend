import { Component, OnInit } from '@angular/core';
import { IEmpresaDesarrolladora, IPlataforma, IVideoJuego } from '../../interfaces/interface';
import { VideojuegosService } from '../../services/videojuegos.service';
import { PlataformaService } from '../../services/plataforma.service';
import { EmpresasService } from '../../services/empresas.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-video-juegos',
  templateUrl: './admin-video-juegos.component.html',
  styleUrls: ['./admin-video-juegos.component.css']
})
export class AdminVideoJuegosComponent implements OnInit {

  juegos: IVideoJuego[] | null | undefined = [];
  juegosFiltados: IVideoJuego[] | null | undefined = [];
  plataformas: IPlataforma[] | null | undefined = [];
  empresas: IEmpresaDesarrolladora[] | null = [];
  

  filtroNombre: string = '';
  filtroPrecio: string = '';
  filtroPlataforma: string = '';
  filtroEmpresa: string = '';

  constructor(private videojuegosService: VideojuegosService,
    private plataformaService: PlataformaService,
    private empresaService: EmpresasService,
    private router: Router,
    
    ) { }

  ngOnInit(): void {

    this.plataformaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.plataformas = response;
      }
    );
    this.empresaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.empresas = response;
      }
    );
    this.videojuegosService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.juegos = response;
        this.juegosFiltados = this.juegos;
      }
    );
  }

  filtrarXNombre() {
    this.filtroPlataforma = '';
    this.filtroPrecio = '';
    this.filtroEmpresa = ''
    this.juegosFiltados = this.juegos?.filter(
      (juego) => {
        return juego.titulo.toUpperCase().includes(this.filtroNombre.toUpperCase());
      }
    );
  }

  filtrarXPrecio() {
    this.filtroPlataforma = '';
    this.filtroEmpresa = ''
    this.filtroNombre = '';
    if (this.filtroPrecio === '') {
      this.juegosFiltados = this.juegos;
      return;
    }
    this.juegosFiltados = this.juegos?.filter(
      (juego) => {
        return juego.precio <= parseInt(this.filtroPrecio);
      }
    );
  }

  filtrarXPlataforma() {
    this.filtroPrecio = '';
    this.filtroEmpresa = ''
    this.filtroNombre = '';
    if (this.filtroPlataforma === '') {
      this.juegosFiltados = this.juegos;
      return;
    }
    this.juegosFiltados = this.juegos?.filter(
      (juego) => {
        return juego.plataforma?.id === parseInt(this.filtroPlataforma);
      }
    );
  }

  filtrarXEmpresa() {
    this.filtroPlataforma = '';
    this.filtroPrecio = '';
    this.filtroNombre = '';
    if (this.filtroEmpresa === '') {
      this.juegosFiltados = this.juegos;
      return;
    }
    this.juegosFiltados = this.juegos?.filter(
      (juego) => {
        return juego.empresaDesarrolladora?.id === parseInt(this.filtroEmpresa);
      }
    );
  }

  onRegistroJuego(){
    this.router.navigate(['formJuego']);
  }

}
