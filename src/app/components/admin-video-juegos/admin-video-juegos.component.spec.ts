import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminVideoJuegosComponent } from './admin-video-juegos.component';

describe('AdminVideoJuegosComponent', () => {
  let component: AdminVideoJuegosComponent;
  let fixture: ComponentFixture<AdminVideoJuegosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminVideoJuegosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminVideoJuegosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
