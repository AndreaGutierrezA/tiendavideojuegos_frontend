import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrmVideoJuegoComponent } from './frm-video-juego.component';

describe('FrmVideoJuegoComponent', () => {
  let component: FrmVideoJuegoComponent;
  let fixture: ComponentFixture<FrmVideoJuegoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrmVideoJuegoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrmVideoJuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
