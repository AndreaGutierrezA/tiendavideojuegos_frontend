import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VideojuegosService } from 'src/app/services/videojuegos.service';
import { PlataformaService } from '../../services/plataforma.service';
import { EmpresasService } from '../../services/empresas.service';
import { IPlataforma } from 'src/app/interfaces/interface';
import { IEmpresaDesarrolladora } from '../../interfaces/interface';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-frm-video-juego',
  templateUrl: './frm-video-juego.component.html',
  styleUrls: ['./frm-video-juego.component.css']
})
export class FrmVideoJuegoComponent implements OnInit {

  validacionForm: FormGroup = new FormGroup({});
  plataformas: IPlataforma[] | null = [];
  empresas: IEmpresaDesarrolladora[] | null = [];

  idJuego : any;

  constructor(
    private fb: FormBuilder,
    private videojuegosService: VideojuegosService,
    private plataformaService: PlataformaService,
    private empresaService: EmpresasService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.crearFormulario();
    this.plataformaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.plataformas = response;
      }
    );
    this.empresaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.empresas = response;
      }
    );
  }

  crearFormulario(): void {
    this.validacionForm = this.fb.group({
      /* id: [this.data === null ? null : this.data.id, []], */
      /* titulo: [this.data ? this.data.titulo : '', [Validators.required]], */
      titulo: ['', [Validators.required]],
      cantidad: ['', [Validators.required]],
      fechaSalida: ['', [Validators.required]],
      precio: ['', [Validators.required]],
      plataforma: ['', [Validators.required]],
      empresaDesarrolladora: ['', [Validators.required]],
    });
  }

  enviarForm(){
    console.log('envio de juego');
    this.videojuegosService.post(this.validacionForm.value).subscribe(
      (juego) => {
       this.idJuego = juego?.id;
        this.toastr.success('Registro Exitoso');
        this.router.navigate(['detalleJuego', this.idJuego]);
      },
      (error) => {
        this.toastr.error('Registro Fallido')
      }
    );
  }

}
