import { Component, OnInit } from '@angular/core';
import { IVideoJuego } from 'src/app/interfaces/interface';
import { Router } from '@angular/router';
import { VideojuegosService } from 'src/app/services/videojuegos.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CarritoCompraService } from 'src/app/services/carrito-compra.service';

@Component({
  selector: 'app-lista-video-juegos',
  templateUrl: './lista-video-juegos.component.html',
  styleUrls: ['./lista-video-juegos.component.css']
})
export class ListaVideoJuegosComponent implements OnInit {

  juegos: IVideoJuego[] | null = [];
  validacionForm: FormGroup = new FormGroup({});

  constructor(
    private router: Router,
    private videojuegosService: VideojuegosService,
    private fb: FormBuilder,
    private carritoCompraService: CarritoCompraService
    ) { }

    ngOnInit(): void {
      this.videojuegosService.getAll().subscribe(
        (response) => {
          console.log(response);
          this.juegos = response;
        }
      );
    }

  detalleJuego(juego: IVideoJuego): void {
    console.log('lista-juegos');
    this.router.navigate(['detalleJuego', juego.id]);
  }
  comprar(cantidad: string, id: number){
    // se auseme que es el usuario 4, puesto que no hay autenticación
    const usuarioId = '4';
    this.videojuegosService.comprar({cantidad, videoJuegoId: id.toString()}).subscribe(
      (response) => {
        console.log('se añadio al carro de compras', response);
        this.carritoCompraService.get(usuarioId).subscribe(
          (resp) => {
            let numeroJuegos = 0;
            resp?.registros.forEach(registro => {
              numeroJuegos += registro.cantidad;
            });
            this.carritoCompraService.setContador(numeroJuegos);
             console.log(resp);
          }
        );
      }, (error) => {
        console.error('error', error)
      }
    );
    console.log(cantidad, id)
  }

}
