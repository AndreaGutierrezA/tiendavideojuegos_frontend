import { Component, OnInit, Input } from '@angular/core';
import { IEmpresaDesarrolladora } from 'src/app/interfaces/interface';
import { Router } from '@angular/router';
import { EmpresasService } from '../../services/empresas.service';

@Component({
  selector: 'app-lista-empresas',
  templateUrl: './lista-empresas.component.html',
  styleUrls: ['./lista-empresas.component.css']
})
export class ListaEmpresasComponent implements OnInit {

  empresas: IEmpresaDesarrolladora[]| null = [];

  constructor(
    private router: Router,
    private empresaService: EmpresasService
  ) { }

  ngOnInit(): void {
    console.log('plataforma');
    this.empresaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.empresas = response;
      }
    );
  }
  litaJuegos(empresa: IEmpresaDesarrolladora): void {
    console.log('lista-juegos');
    this.router.navigate(['juego', empresa.id]);
  }

  detalleEmpresa(empresa: IEmpresaDesarrolladora) {
    this.router.navigate(['detalleEmpresa', empresa.id]);
  }

}
