import { Component, OnInit } from '@angular/core';
import { CarritoCompraService } from '../../services/carrito-compra.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carrito-compra',
  templateUrl: './carrito-compra.component.html',
  styleUrls: ['./carrito-compra.component.css']
})
export class CarritoCompraComponent implements OnInit {

  contador: number = 0;
  constructor(private carritoCompraService: CarritoCompraService,
    private router: Router,) { }

  ngOnInit(): void {
    this.carritoCompraService.contador.subscribe(
      (contador) => {
        this.contador = contador;
      }
    )
  }
  lista() {
    this.router.navigate(['listaCarrito']);
  }

}
