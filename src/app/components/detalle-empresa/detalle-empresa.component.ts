import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IEmpresaDesarrolladora } from '../../interfaces/interface';

@Component({
  selector: 'app-detalle-empresa',
  templateUrl: './detalle-empresa.component.html',
  styleUrls: ['./detalle-empresa.component.css']
})
export class DetalleEmpresaComponent implements OnInit {

  idEmpresa: string = '';
  empresa: IEmpresaDesarrolladora[] = [];
  /* parametros: Subscription; */
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {

    /* this.parametros = this.activatedRoute.params.subscribe((params: Params) => {
      this.idEmpresa = params.idEmpresa;
    }); */
  }

  onRegresar(): void {
    this.router.navigate(['adminVideoJuegos']);
  }



}
