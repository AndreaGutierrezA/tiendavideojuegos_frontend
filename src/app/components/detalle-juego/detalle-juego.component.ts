import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IVideoJuego } from 'src/app/interfaces/interface';
import { VideojuegosService } from 'src/app/services/videojuegos.service';

@Component({
  selector: 'app-detalle-juego',
  templateUrl: './detalle-juego.component.html',
  styleUrls: ['./detalle-juego.component.css']
})
export class DetalleJuegoComponent implements OnInit {

  parametros: Subscription = new Subscription();
  juego: IVideoJuego | null = {
    id: 0,
    titulo: '',
    cantidad: 0,
    fechaSalida: '',
    precio: 0,
  };

  idJuegos: any = 0;
  constructor(
    private router: Router,
    private videojuegosService: VideojuegosService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.parametros = this.activatedRoute.params.subscribe((params: Params) => {
      this.idJuegos = params['idJuego'];
    });

    this.videojuegosService.getOne(this.idJuegos).subscribe(
      (response) => {
        console.log(response);
        this.juego = response;
      }
    );
  }

  onRegresar(){
    this.router.navigate(['adminVideoJuegos']);
  }
}
