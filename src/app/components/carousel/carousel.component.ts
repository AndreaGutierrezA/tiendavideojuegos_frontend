import { Component, Input, OnInit } from '@angular/core';
import { IVideoJuego } from '../../interfaces/interface';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  @Input() juegos: IVideoJuego[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
