import { Component, OnInit } from '@angular/core';
import { CarritoCompraService } from '../../services/carrito-compra.service';
import { ICarrito } from '../../interfaces/interface';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-lista-carrito',
  templateUrl: './lista-carrito.component.html',
  styleUrls: ['./lista-carrito.component.css']
})
export class ListaCarritoComponent implements OnInit {

  total: number = 0;
  carrito: ICarrito | null = {
    despachado: false,
    id: '',
    registros: []
  };

  constructor(private carritoCompraService: CarritoCompraService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    // se asume que es el usuario 4 ua que noj hay autenticación
    const usuarioId = '4';
    this.consultarCarrito(usuarioId);

  }
  consultarCarrito(usuarioId: string) {
    this.carritoCompraService.get(usuarioId).subscribe(
      (carrito) => {
        this.carrito = carrito;
        this.carrito?.registros.forEach(
          (registro) => {
            this.total += registro.precioTotal;
          }
        )
      }
    )
  }

  confirmar(id: string | undefined) {
    this.carritoCompraService.put(id).subscribe(
      (response) => {
        this.toastr.success('Compra Exitosa');
        // se asume que es el usuario 4 ua que noj hay autenticación
        const usuarioId = '4';
        this.consultarCarrito(usuarioId);
        this.total = 0;
        this.carritoCompraService.setContador(0);
      }
    )
  }

  eliminar(id: string) {
    this.carritoCompraService.delete(id).subscribe(
      () => {
        this.toastr.success('Registro Eliminado');
        // se asume que es el usuario 4 ua que noj hay autenticación
        const usuarioId = '4';

        this.carritoCompraService.get(usuarioId).subscribe(
          (factura) => {
            let numeroJuegos = 0;
            this.carrito = factura;
            factura?.registros.forEach(registro => {
              numeroJuegos += registro.cantidad;
            });
            this.carritoCompraService.setContador(numeroJuegos);
          }
        )
      }
    )
  }

}
