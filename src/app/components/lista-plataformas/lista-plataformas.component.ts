import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IPlataforma } from 'src/app/interfaces/interface';
import { PlataformaService } from '../../services/plataforma.service';

@Component({
  selector: 'app-lista-plataformas',
  templateUrl: './lista-plataformas.component.html',
  styleUrls: ['./lista-plataformas.component.css']
})
export class ListaPlataformasComponent implements OnInit {

  @Input() plataformas: IPlataforma[] | null = [];
  constructor( private router: Router, private plataformaService: PlataformaService) {}

  ngOnInit(): void {
    console.log('plataforma');
    this.plataformaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.plataformas = response;
      }
    );
  }

  onDetalle(plataforma: IPlataforma): void{
    console.log('empresa');
    this.router.navigate(['empresa', plataforma.id]);
  }

}
