export interface IPlataforma {
    id: number;
    nombre: string;
}
export interface IEmpresaDesarrolladora {
    id: number;
    nombre: string;
    descripcion: string;
    año: string;
    nit: string;
}
export interface IVideoJuego {
    id: number;
    titulo: string;
    cantidad: number;
    fechaSalida: string;
    precio: number;
    plataforma?: IPlataforma;
    empresaDesarrolladora?: IEmpresaDesarrolladora;
}
export interface IUsuario {
    id: number;
    nro_identificacion: string;
    nombre: string;
    apellidos: string;
    celular: string;
    correo: string;
}

export interface ICarrito {
    id: string;
    despachado: boolean;
    registros: IRegistro[];
}
export interface IRegistro {
    id: string;
    cantidad: number;
    fecha: string;
    precioTotal: number;
    videoJuego: IVideoJuego;
}