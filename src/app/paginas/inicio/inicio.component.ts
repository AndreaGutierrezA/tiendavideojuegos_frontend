import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpresasService } from 'src/app/services/empresas.service';
import { PlataformaService } from 'src/app/services/plataforma.service';
import { VideojuegosService } from 'src/app/services/videojuegos.service';
import { IEmpresaDesarrolladora, IPlataforma,  IVideoJuego } from '../../interfaces/interface';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  
  juegos: IVideoJuego[] | null = [];
  plataformas: IPlataforma[] | null = [];
  empresas: IEmpresaDesarrolladora[] | null = [];

  constructor(
    private videojuegosService: VideojuegosService,
    private plataformaService: PlataformaService,
    private empresaService: EmpresasService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.plataformaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.plataformas = response;
      }
    );
    this.empresaService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.empresas = response;
      }
    );
    this.videojuegosService.getAll().subscribe(
      (response) => {
        console.log(response);
        this.juegos = response;
      }
    );
  }

}
