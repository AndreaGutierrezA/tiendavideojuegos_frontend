import { Component, OnInit } from '@angular/core';
import { CarritoCompraService } from './services/carrito-compra.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tiendaVideoJuegos';

  constructor(
    private carritoCompraService: CarritoCompraService
  ) { }

  ngOnInit(): void {
    // se asume que es el usuario 4 ua que noj hay autenticación
    const usuarioId = '4';
    this.carritoCompraService.get(usuarioId).subscribe(
      (factura) => {
        let numeroJuegos = 0;
        factura?.registros.forEach(registro => {
          numeroJuegos += registro.cantidad;
        });
        this.carritoCompraService.setContador(numeroJuegos);
      }
    )
  }
}
